# -*- coding: utf-8 -*-
import os
import sys
import unittest

cmd_folder = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
if cmd_folder not in sys.path:
    sys.path.insert(0, cmd_folder)

import bmodels as models
import datetime


class Mark(models.Model):
    uid = models.UnsignedInt32Field()
    device_id = models.UnsignedInt16Field()
    is_valid = models.UnsignedInt8Field()
    altitude = models.Int32Field()
    altitude_high = models.Int32Field(convert_to=lambda x: x * 10, convert_from=lambda x: x / 10.0)
    speed = models.FloatField()
    azimuth = models.FloatField()
    longitude = models.FloatAsUInt32Field(multiplier=10 ** 7)
    latitude = models.FloatAsUInt32Field(multiplier=10 ** 7)
    datetime = models.DateTimeField()


class ModelTestCase(unittest.TestCase):
    def test_simple_dump(self):
        mark = Mark()
        mark.uid = 912039214
        mark.device_id = 24456
        mark.is_valid = 1
        mark.altitude = -400
        mark.altitude_high = 11
        mark.speed = 91.563454
        mark.azimuth = 123.345556
        mark.longitude = 65.56
        mark.latitude = 57.75
        mark.datetime = datetime.datetime(2012, 12, 31, 1, 59, 59, )

        data = mark.dumpb()

        restored_mark = Mark()
        restored_mark.loadb(data)

        self.assertEqual(restored_mark.uid, mark.uid)
        self.assertEqual(restored_mark.device_id, mark.device_id)
        self.assertEqual(restored_mark.is_valid, mark.is_valid)
        self.assertEqual(restored_mark.altitude, mark.altitude)
        self.assertEqual(restored_mark.altitude_high, mark.altitude_high)
        self.assertAlmostEqual(restored_mark.speed, mark.speed, places=5)
        self.assertAlmostEqual(restored_mark.azimuth, mark.azimuth, places=5)
        self.assertAlmostEqual(restored_mark.latitude, mark.latitude, places=5)
        self.assertAlmostEqual(restored_mark.longitude, mark.longitude, places=5)
        self.assertEqual(restored_mark.datetime, mark.datetime)


if __name__ == '__main__':
    unittest.main()
